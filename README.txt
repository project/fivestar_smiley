This module provides a set of smileys that may be used with Fivestar.
It has two sizes of smileys: 16x16 and 32x32.

To use the module, simply enable it and go to "manage display" on your
content type and choose the smileys in "star display options" on your
Fivestar Rating field.

The smiley images are created by Jørgen Bo Wind.

In the image folders you find 5 smileys
1.png: very happy
2.png: medium happy
3.png: between happy
4.png: medium angry
5.png: very mad

The Danish union PROSA has developed this module and the artwork.
For more information about PROSA see:
  PROSA: Forbundet af It-professionelle
  http://www.prosa.dk/
