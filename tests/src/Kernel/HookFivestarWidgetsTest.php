<?php

namespace Drupal\Tests\fivestar_smiley\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the operation of hook_fivestar_widgets().
 *
 * @group fivestar_smiley
 */
class HookFivestarWidgetsTest extends KernelTestBase {

  /**
   * The Fivestar widget manager.
   *
   * @var \Drupal\fivestar\WidgetManager
   */
  protected $widgetManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'votingapi',
    'fivestar',
    'fivestar_smiley',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->widgetManager = $this->container->get('fivestar.widget_manager');
  }

  /**
   * Tests finding widgets defined by hook_fivestar_widgets().
   */
  public function testWidgetDiscovery(): void {
    $expected = [
      // Smileys 16x16 is defined in the fivestar_smiley module.
      'smiley16' => [
        'library' => 'fivestar_smiley/smileys_16x16',
        'label' => 'Smileys 16x16',
      ],
      // Smileys 32x32 is defined in the fivestar_smiley module.
      'smiley32' => [
        'library' => 'fivestar_smiley/smileys_32x32',
        'label' => 'Smileys 32x32',
      ],
    ];

    // Invoke the hook and collect all defined widgets.
    $widgets = $this->widgetManager->getWidgets();

    // Verify "Smileys 16x16" was discovered.
    $this->assertArrayHasKey('smiley16', $widgets);
    $this->assertEquals($expected['smiley16']['label'], $widgets['smiley16']['label']);
    $this->assertEquals($expected['smiley16']['library'], $widgets['smiley16']['library']);

    // Verify "Smileys 32x32" was discovered.
    $this->assertArrayHasKey('smiley32', $widgets);
    $this->assertEquals($expected['smiley32']['label'], $widgets['smiley32']['label']);
    $this->assertEquals($expected['smiley32']['library'], $widgets['smiley32']['library']);
  }

}
